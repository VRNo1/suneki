# README #

* * *
### Presentation ###

Suneki is a 3D game developed with Unity3D and using C# scripting.

This is currently a simple clone of the game "Snake", adapted in 3D world.

Actual version is alpha 0.2, see log at end of file.

The gameplay differs little from the original: 

* Eat blocks to grow up and score
* Eating blocks increase speed. You can see your energy power.
* Don't touch a wall or the snake's tail
* Random blocks bar the way and set up difficulty.
* * *
### Setup ###
Download project and run with Unity3D.

Play with keyboard or joystick. See Control section after screenshots.
* * *
### Incoming ###
* Energy will become the center of gameplay
* More Gameplay adjustments (rotations when speedup)
* A real scoring system
* Improved map generator
* Various game modes
* Multiplayer (Local and Online)
* Better graphics
* Sound !
* * *
### Contact ###
caupetit@student.42.fr

tmielcza@student.42.fr
* * *
### Screenshots ###
Alpha 0.1, now differs a bit
![Suneki_1.jpg](https://bitbucket.org/repo/jGL9kn/images/386165079-Suneki_1.jpg)
* * *
![Snake3.jpg](https://bitbucket.org/repo/jGL9kn/images/745557704-Snake3.jpg)
* * *

Controls:

* Arrows/stick left to move.
* WASD (qwerty)/stick right - shift moves.
* Return or Joy0 - (X or A) confirm your choice on death gui.
* Escape - quit game at any time.
* * *

Log file:

Version Alpha 0.2

* Eating blocks now gives energy power.
* Eating blocks makes snake grow up with 2 blocks. So tail is more dangerous for you !
* Energy power increase speed.
* Optimisations on tail moves and map generation.
* Added joystick control.
* You can use shift move. It allows you to dodge blocks or eat easily.
* Gui on death can now be managed with joystick keyboard and mouse.