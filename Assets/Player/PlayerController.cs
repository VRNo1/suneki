﻿using UnityEngine;
using System.Collections;

public class PlayerController : MonoBehaviour
{
    public bool isAlive;

    private Score score;

    void Awake()
    {
        isAlive = true;
    }

    void Start()
    {
        score = this.GetComponent<Score>();
    }

    public void Eat()
    {
        score.score += 1;
        GameObject.Find("Snake1").GetComponent<EnergyController>().PowerUp();
    }

    public void Die()
    {
        isAlive = false;
    }

    public void Restart()
    {
        isAlive = true;
        score.score = 0;
    }
}
