﻿using UnityEngine;
//using UnityEditor;
using System.Collections;

/// <summary>
/// Animates a snake chunk.
/// Must be attached to a TailChunk.
/// </summary>
public class AnimTruc : MonoBehaviour
{

    // The Snake which the TailChunk is attached to.
    private GameObject snake;

    // Tail script attached to snake.
    private Tail tail;

    private PlayerController player;

    // Contains snake moves.
    private AnimationClip clip;

    private AnimationState clipAnimState;

    private move snakeMove;

    // Current location in the animation.
    private float timer;

    // Initiates all we need for animation.
    void Awake()
    {
        animation.wrapMode = WrapMode.Once;
        player = GameObject.Find("PlayerController").GetComponent<PlayerController>();
    }

    void Start()
    {
        snake = GameObject.Find(this.name.Substring(5));
        snakeMove = snake.GetComponent<move>();
        tail = snake.GetComponent<Tail>();
        clip = tail.clip;
        timer = tail.animationTime - tail.chunkTimeOffset * tail.size;
        animation.AddClip(clip, clip.name);
        clipAnimState = animation[clip.name];
        clipAnimState.time = timer;
        clipAnimState.speed = snakeMove.speedCoef;
        animation.Play(clip.name);
    }

    // Plays animation if player is alive.
    void Update()
    {
        if (!player.isAlive)
        {
            clipAnimState.speed = 0f;
            return;
        }
        clipAnimState.speed = snakeMove.speedCoef;
    }
}
