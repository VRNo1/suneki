﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Generate food on map.
/// Use it in GameController.
/// </summary>
public class FoodGenerator : MonoBehaviour
{
    /// <summary>
    /// Add it in Unity. Represents food.
    /// </summary>
    public Transform foodPrefab;

    /// <summary>
    /// Food generation data
    /// </summary>
    public Entity food;

    /// <summary>
    /// Max number of food in map;
    /// </summary>
    public int foodMax = 60;

    /// <summary>
    /// food number on map at beginnig.
    /// </summary>
    public int foodInitial = 10;

    /// <summary>
    /// number of food to add after each eat;
    /// </summary>
    public int foodIncrease = 1;

    // Map area.
    private BoxBounds boxBounds;

    void Awake()
    {
        food = new Entity(foodInitial, foodPrefab);
    }

    void Start()
    {
        boxBounds = this.GetComponent<MapGeneratorScript>().boxBounds;
    }

    void Update()
    {
        Entity.Generate(food, boxBounds);
    }

    public void Reset()
    {
        food.CreatedNb = 0;
    }

    /// <summary>
    /// Update food number and increase maxFood if needed
    /// </summary>
    public void UpdateGeneration()
    {
        food.CreatedNb--;
        if (food.Max < foodMax)
            food.Max += foodIncrease;
    }
}
