﻿using UnityEngine;
using System.Collections;

public class SnakeGenerator : MonoBehaviour
{
    /// <summary>
    /// Add it in unity. Represents the players.
    /// </summary>
    public Transform snakePrefab;

    /// <summary>
    /// Player generation data.
    /// </summary>
    public Entity snake;

    // Map area.
    public BoxBounds boxBounds;

    void Awake()
    {
        snake = new Entity(1, snakePrefab);
    }

    void Start()
    {
        boxBounds = this.GetComponent<MapGeneratorScript>().boxBounds;
    }

    void Update()
    {
        Entity.Generate(snake, boxBounds);
    }
}
