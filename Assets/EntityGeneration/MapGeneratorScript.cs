﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Generates Map According to box scale.
/// Contains boxbounds.
/// Add it in GameController.
/// </summary>
public class MapGeneratorScript : MonoBehaviour
{
    /// <summary>
    /// Scale of the map.
    /// </summary>
    public Vector3 mapScale;

    /// <summary>
    /// Add it in unity. Represents obstacles in the map.
    /// </summary>
    public Transform barrierPrefab;

    /// <summary>
    /// Max number of barrier to generate.
    /// </summary>
    public int barriersMax = 300;

    /// <summary>
    /// Barriers generation data.
    /// </summary>
    public Entity barriers;

    /// <summary>
    /// The box given limits af the map.
    /// </summary>
    public GameObject box;

    /// <summary>
    /// map area bounds data
    /// </summary>
    public BoxBounds boxBounds;

    void Awake()
    {
        box = GameObject.FindGameObjectWithTag(Tags.box);
        box.transform.localScale = mapScale;
        boxBounds = new BoxBounds(box);
        barriers = new Entity(barriersMax, barrierPrefab);
    }

    void Start()
    {
        Entity.Generate(barriers, boxBounds);
    }
}
