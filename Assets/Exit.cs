﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Provides tool to exit program.
/// Add it to the Box gameobject in unity.
/// </summary>
public class Exit : MonoBehaviour
{
	// Exit program when 'escape' is pressed.
	void Update ()
	{
		if (Input.GetKey(KeyCode.Escape))
			Application.Quit();
	}
}
