﻿using UnityEngine;
using System.Collections;

public class GuiButton
{
    public string name;
    public Rect rect;
    public delegate void action();
    public action func;

    public GuiButton(string name, Rect rect, action func)
    {
        this.name = name;
        this.rect = rect;
        this.func = func;
    }
}
